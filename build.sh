#!/usr/bin/env bash

if [[ $(uname) = Darwin ]]; then
    cachedir="$HOME/Library/Caches/KBuild"
else
    if [[ -z $XDG_DATA_HOME ]]; then
        cachedir="$HOME/.local/share"
    else
        cachedir="$XDG_DATA_HOME"
    fi
fi
mkdir -p $cachedir

url="https://www.nuget.org/nuget.exe"

# TODO: Use "NuGet.exe Update" to self update the exe to the latest version.
if [[ ! -f "$cachedir/nuget.exe" ]]; then
    echo "Downloading latest version of NuGet.exe..."
    wget -O "$cachedir/nuget.exe" "$url" 2>/dev/null || curl -o $cachedir/nuget.exe --location $url /dev/null
fi

# Install nuget.exe tool
if [[ ! -e ".nuget" ]]; then
    mkdir .nuget
    cp -- "$cachedir/nuget.exe" ".nuget/nuget.exe"
fi

# Install Sake and KoreBuild packages
if [[ ! -d packages/KoreBuild ]]; then
    mono .nuget/nuget.exe install KoreBuild -ExcludeVersion -o packages -nocache -pre
    mono .nuget/nuget.exe install Sake -version 0.2 -o packages -ExcludeVersion
    #mono --runtime=v4.0 "./.nuget/NuGet.exe" install Sake -pre -o packages
fi

if [[ ! type k > /dev/null 2>&1 ]]; then
    source packages/KoreBuild/build/kvm.sh
    kvm upgrade
fi

# Kick of Sake build passing argument(s)
#export EnableNuGetPackageRestore=true
mono packages/Sake/tools/Sake.exe -I packages/KoreBuild/build -f makefile.shade "$@"
#mono $(find packages/Sake.*/tools/Sake.exe|sort -r|head -n1) -I packages/KoreBuild/build -f makefile.shade "$@"
