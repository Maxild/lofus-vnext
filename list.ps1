$searchTerm = $args[0]
$versionTerm = $null
if ($args.Length -gt 1) {
    $versionTerm = $args[1]
}


function Get-Dirname ($path) { Split-Path -parent $path }

$rootFolder = Get-Dirname "$PSCommandPath"

$nugetConfigs="Nuget.developer.config", "Nuget.release.config", "Nuget.master.config", "Nuget.config"
#$nugetConfigs="Nuget.master.config"

write-host "Listing packages matching '$searchTerm' from feeds in $nugetConfigs"

foreach ($name in $nugetConfigs) {

    $nugetConfig = Join-Path $rootFolder $name

    $feeds = Select-Xml -Path $nugetConfig -XPath "/configuration/packageSources/add/@value" |
            select -expand Node | select -expand '#text'

    $nugetExe = Join-Path $rootFolder ".nuget\nuget.exe"

    write-host "ASP.NET vNext feed defined in '$name' ($feeds)"

    $output = Invoke-Expression "$nugetExe list $searchTerm -source $feeds -verbosity normal -allversions -pre"

    # versionTerm => exact match of name, because of single whitespace before versionTerm
    if ($versionTerm -eq $null) {
        $output | foreach { Write-Host $_ }
    }
    else {
        [regex]$package = "^$([regex]::Escape($searchTerm))(\s){1}$([regex]::Escape($versionTerm))"
        $output | foreach {
            if ($_ -match $package) {
                Write-Host $_
            }
        }
    }
}

