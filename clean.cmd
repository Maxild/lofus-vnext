SET LOFUS_PATH=src\Brf.Lofus.Core\

REM cleaning all the generated files (and not installed nuget packages)
::rmdir /S /Q .nuget
::rmdir /S /Q packages
rmdir /S /Q bin
rmdir /S /Q %LOFUS_PATH%\bin
rmdir /S /Q %LOFUS_PATH%\obj
