@echo off
cd %~dp0

SETLOCAL
SET CACHED_NUGET="%LocalAppData%\NuGet\NuGet.exe"

IF EXIST %CACHED_NUGET% goto copynuget
echo Downloading latest version of NuGet.exe...
IF NOT EXIST "%LocalAppData%\NuGet" md "%LocalAppData%\NuGet"
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "$ProgressPreference = 'SilentlyContinue'; Invoke-WebRequest 'https://www.nuget.org/nuget.exe' -OutFile '%CACHED_NUGET%'"

:copynuget
IF EXIST .nuget\nuget.exe goto restore
md .nuget
copy %CACHED_NUGET% .nuget\nuget.exe > nul

:restore
IF EXIST packages\KoreBuild goto sake
:: install KoreBuild (kvm tool)
.nuget\NuGet.exe install KoreBuild -ExcludeVersion -o packages -nocache -pre

:sake
IF EXIST packages\Sake.0.2 goto build
:: install Sake
.nuget\NuGet.exe install Sake -version 0.2 -o packages -ExcludeVersion

:build
:: Kick of Sake build passing argument(s)
::packages\Sake.0.2\tools\Sake.exe -I packages\KoreBuild\build -f makefile.shade %*
packages\Sake.0.2\tools\Sake.exe -f learntomake.shade %*
