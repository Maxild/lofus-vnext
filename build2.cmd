@echo off
:: See http://www.microsoft.com/resources/documentation/windows/xp/all/proddocs/en-us/percent.mspx?mfr=true
SET ROOT_DIR=%~dp0
cd %ROOT_DIR%

::Remove any trailing slash
IF "%ROOT_DIR:~-1%"=="\" SET ROOT_DIR=%ROOT_DIR:~,-1%

SET SRC_DIR=%ROOT_DIR%\src
SET TEST_DIR=%ROOT_DIR%\test

echo root-dir: %ROOT_DIR%
echo src-dir:  %SRC_DIR%
echo test-dir: %TEST_DIR%
@echo off


:: The first part of the build does ensure (bootstrap) having
:: the (full/desktop) CLR and the (cloud based)CoreCLR runtime
:: on the build machine (local machine or dedicatd build server).

SETLOCAL
SET CACHED_NUGET="%LocalAppData%\NuGet\NuGet.exe"

IF EXIST %CACHED_NUGET% goto copynuget
echo Downloading latest version of NuGet.exe...
IF NOT EXIST "%LocalAppData%\NuGet" md "%LocalAppData%\NuGet"
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "$ProgressPreference = 'SilentlyContinue'; Invoke-WebRequest 'https://www.nuget.org/nuget.exe' -OutFile '%CACHED_NUGET%'"

:copynuget
IF EXIST .nuget\nuget.exe goto korebuild
md .nuget
copy %CACHED_NUGET% .nuget\nuget.exe > nul

:korebuild
:: install KoreBuild (kvm tool)
:: latest version does not work right now...use list.ps1 'KoreBuild'
:: to see all versions (KoreBuild 0.2 to see specific version..prefix)
SET SDK_VERSION="0.2.1-beta2-10061"
CALL .nuget\nuget.exe install KoreBuild -Source https://www.myget.org/F/aspnetrelease/api/v2 -Version %SDK_VERSION% -ExcludeVersion -o packages -nocache -pre

:kre
:: Install KRE (kpm, k)
IF "%SKIP_KRE_INSTALL%"=="1" goto clean
::CALL packages\KoreBuild\build\kvm list
::CALL packages\KoreBuild\build\kvm alias default 1.0.0-beta2
CALL packages\KoreBuild\build\kvm upgrade -runtime CLR -x86
CALL packages\KoreBuild\build\kvm install default -runtime CoreCLR -x86
:: How can I build and test for both CLR and CoreCLR???
CALL packages\KoreBuild\build\kvm use default -runtime CLR -x86

:: Note: At this point the latest KRE is known to be installed on the build machine
:: The rest of this build script should be written in another language

:clean
echo Clean
echo =====
:: Remove all bin directories under ROOT_DIR
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "ls '%ROOT_DIR%' bin -Directory -Recurse -erroraction 'silentlycontinue' | rm -Force -Recurse; exit $Lastexitcode"

:kpm_restore
echo Restore
echo =======
:: Run kpm restore in every directory under ROOT_DIR that has a global.json
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "ls '%ROOT_DIR%' global.json -Recurse -erroraction 'silentlycontinue'  | Select-Object -Expand DirectoryName | Foreach { cmd /C cd $_ `&`& CALL kpm restore }; exit $Lastexitcode"

:buildall
:: TODO: Build for all runtimes (CoreCLR and CLR) and target frameworks
echo Build
echo =====
:: Run kpm build project.json on every project.json file
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "ls '%ROOT_DIR%' project.json -Recurse -erroraction 'silentlycontinue' | Foreach { kpm build $_.FullName }; exit $Lastexitcode"
:: Show nuget packages (nupkg's) created by this build
echo Artifacts
echo =========
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "ls '%SRC_DIR%' *.nupkg -Recurse -erroraction 'silentlycontinue' | Where-Object {$_.FullName -match 'bin'} | Select-Object -Expand FullName | Foreach { Write-Host `'$_`' }; exit $Lastexitcode"

:testall
:: TODO: Test for all runtimes (CoreCLR and CLR)
echo Running Tests
echo =============
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "ls %TEST_DIR% project.json -Recurse -erroraction 'silentlycontinue' | Where-Object { $_.FullName -like '*test*' } | Select-Object -Expand DirectoryName | Foreach { cmd /C cd $_ `&`& k test }; exit $Lastexitcode"
